package qa.poc.conf;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigManager {
	
	static InputStream input;
	Properties prop = new Properties();
	
	public String getUrl(String basePath, String endPoint) throws IOException {
		String url = null;
		try {
			input = new FileInputStream("./src/main/resources/conf/config.properties");
			prop.load(input);
			url = prop.getProperty(basePath)+prop.getProperty(endPoint);
		} catch (FileNotFoundException e) {
			System.out.println("'config.properties' file does not exist in the specified location");
			e.printStackTrace();
		}
		return url;
	}
}
