package qa.poc.script.api.services.auth;

import io.restassured.response.Response;
import qa.poc.adapters.RestAssuredWrapper;
import qa.poc.utils.ASCIIArtGenerator;
import qa.poc.utils.ASCIIArtGenerator.ASCIIArtFont;

public class Login {
	
	public Response login(String reqBody, String reqUrl) throws Exception {
		RestAssuredWrapper rest = new RestAssuredWrapper();
		System.out.println("\n\t*********************************** LOGIN ***********************************");
		System.out.println("\tRequest URL: \n\t" +reqUrl);
		System.out.println("\n\tRequest Body: \n\t" +reqBody);
		Response res = rest.postRequest(reqUrl, reqBody);
		
		return res;
	}
}
