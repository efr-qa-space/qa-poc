package qa.poc.script.api.services.management;

import io.restassured.response.Response;
import qa.poc.adapters.RestAssuredWrapper;

public class GetTemporaryKey {

	public Response getTempKey(String reqUrl, String token) {
		
		RestAssuredWrapper rest = new RestAssuredWrapper();
		System.out.println("\n\t*********************************** Get Temporary Key ***********************************");
		System.out.println("\tRequest URL: \n\t" +reqUrl);
		
		Response res = rest.getRequest(reqUrl, token);
		
		return res;
		
	}
}
