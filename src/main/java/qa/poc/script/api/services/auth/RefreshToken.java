package qa.poc.script.api.services.auth;

import java.io.IOException;

import io.restassured.response.Response;
import qa.poc.adapters.RestAssuredWrapper;
import qa.poc.conf.ConfigManager;

public class RefreshToken {
	
	public Response refreshToken(String reqBody) throws Exception {
		String reqUrl;
		RestAssuredWrapper restApi = new RestAssuredWrapper();	
		ConfigManager config = new ConfigManager();
		
		reqUrl = config.getUrl("RH-Gateway", "RefreshToken");
		Response res = restApi.postRequest(reqUrl, null, reqBody);
		
		return res;
	}

}
