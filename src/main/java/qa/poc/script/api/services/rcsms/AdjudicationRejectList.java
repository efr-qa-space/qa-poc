package qa.poc.script.api.services.rcsms;

import io.restassured.response.Response;
import qa.poc.adapters.RestAssuredWrapper;

public class AdjudicationRejectList {
	
	public Response adjRejectList(String reqBody, String reqUrl, String token, String testDesc) {
		
		RestAssuredWrapper rest = new RestAssuredWrapper();
		System.out.println("\n\t*********************************** ADJUDICATION - "+testDesc+" ***********************************");
		System.out.println("\tRequest URL: \n\t" +reqUrl);
		System.out.println("\n\tRequest Body: \n\t" +reqBody);
		Response res = rest.postRequest(reqUrl, token, reqBody);
		
		return res;
	}

}