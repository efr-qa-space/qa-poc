package qa.poc.script.api.testcases.common.onboarding;

import java.util.LinkedHashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.response.Response;
import qa.poc.conf.ConfigManager;
import qa.poc.script.api.services.rcsms.RCSMSOnboarding;
import qa.poc.utils.ReadJsonDataFile;
import qa.poc.utils.ReadRequestBody;

public class RCSMSOnboardingTest {

	RCSMSOnboarding rcsms = new RCSMSOnboarding();
	ConfigManager config = new ConfigManager();
	ReadJsonDataFile readJson = new ReadJsonDataFile();
	ReadRequestBody readReq = new ReadRequestBody();
	public LinkedHashMap<String, String> jsonTestData;

	@Test
	public Response rcsmsOnboardingTest(String jsonDataFile, String token, String testDesc) throws Exception {

		//declaration
		jsonTestData = readJson.getDataFromJson(jsonDataFile);
		String basePath = jsonTestData.get("basePath");
		String endPoint = jsonTestData.get("onboardingEndPoint");
		String reqUrl = config.getUrl(basePath, endPoint);
		String reqBody = readReq.getContent(jsonTestData.get(testDesc));
		
		//Sending POST request
		Response res = rcsms.onboarding(reqBody, reqUrl, token, testDesc);
		Boolean resBodyExist = res.getHeaders().hasHeaderWithName("Content-Type");
		if(resBodyExist) 
		System.out.println("\n\tResponse Body: \n\t" + res.asString());

		// Response status code validation
		int statusCode = res.getStatusCode();
		if (statusCode != 200 && resBodyExist) {
			Assert.assertEquals(statusCode, 200,
					"\n\tOnboarding request failed: Status Code- " + statusCode + "\n\t Error Code- "
							+ res.path("errors.code") + "]\n\t Error Message- " + res.path("errors.message") + "\n\t");
		} else {
			Assert.assertEquals(statusCode, 200,
					"\n\tOnboarding request failed: Status Code- " + statusCode +"\n\t");
		}

		//Returns response for validation
		return res;
	}
}
