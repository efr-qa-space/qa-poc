package qa.poc.script.api.testcases.rcsms;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.response.Response;
import qa.poc.script.api.testcases.common.adjudication.AdjudicationRejectListTest;
import qa.poc.script.api.testcases.common.auth.LoginTest;
import qa.poc.script.api.testcases.common.management.GetTempKeyTest;
import qa.poc.script.api.testcases.common.onboarding.RCSMSOnboardingTest;

public class RCSMSRegressionTest {

	public String token;
	// Mention Test Data json file name below
	public String jsonDataFile = "RCSMSRegressionTestData.json";
	
	// Instantiation of Test class
	LoginTest login = new LoginTest();
	GetTempKeyTest tempKey = new GetTempKeyTest();
	RCSMSOnboardingTest rcsms = new RCSMSOnboardingTest();
	AdjudicationRejectListTest arl = new AdjudicationRejectListTest();

	//Method to call Tests
	public void RCSMSRegression() throws Exception {

		rcsmsLoginTest();
		rcsmsGetTempKeyTest();
		rcsmsOnboardingMatchWithEIDTest();
		rcsmsOnboardingNoMatchWithEIDTest();
		rcsmsOnboardingNoDatabaseWithEIDTest();
		rcsmsOnboardingNoDbNfcMatchWithEIDTest();
		rcsmsOnboardingMatchWithPassportTest();
		rcsmsOnboardingNoMatchWithPassportTest();
		rcsmsOnboardingNoDatabaseWithPassportTest();
		rcsmsOnboardingNoDbNfcMatchWithPassportTest();
		rcsmsAdjudicationRejList();
	}

//<****************Test Methods*******************>
	// priority levels are set for sequential execution of tests

	@Test(priority = 1)
	// Login for RCSMS
	public void rcsmsLoginTest() throws Exception {
		
		// calling LoginTest in package authentication
		token = login.loginTest(jsonDataFile);
	}

	@Test(priority = 2)
	// Get Temporary Key for RCSMS
	public void rcsmsGetTempKeyTest() throws Exception {

		// calling GetTempKeyTest in package management
		int statusCode = tempKey.getTempKeyTest(jsonDataFile, token);
		if (statusCode!=200) {
			Assert.fail("Failed to Get Temporary Key!!!");
		}
	}
	
//<****************EID Scenarios*******************>
	@Test
	// RCSMS test for Onboarding - MatchWithEID
	public void rcsmsOnboardingMatchWithEIDTest() throws Exception {

		// Placeholder given for request body filename in respective json data file.
		// Also printed in console log as test description to differentiate tests
		String testDesc = "MatchWithEID";

		// Sending POST request
		Response res = rcsms.rcsmsOnboardingTest(jsonDataFile, token, testDesc);

		// Response body validation
		if (res.path("result.code").equals(1) && res.path("result.message").equals("MATCH")) {
			
			System.out.println("Onboarding test passed successfully!");
		} else {
			
			Assert.fail("\n\tOnboarding request failed!\n\t" + res.path("result.message") + "\nUnexpected Response!!!");
		}
	}

	@Test
	// RCSMS test for Onboarding - NoMatchWithEID
	public void rcsmsOnboardingNoMatchWithEIDTest() throws Exception {

		// Placeholder given for request body filename in respective json data file.
		// Also printed in console log as test description to differentiate tests
		String testDesc = "NoMatchWithEID";

		// Sending POST request
		Response res = rcsms.rcsmsOnboardingTest(jsonDataFile, token, testDesc);

		// Response body validation
		if (res.path("result.code").equals(0) && res.path("result.message").equals("NO MATCH")) {
			
			System.out.println("Onboarding test passed successfully!");
		} else {
			
			Assert.fail("\n\tOnboarding request failed!\n\t" + res.path("result.message") + "\nUnexpected Response!!!");
		}
	}

	@Test
	// RCSMS test for Onboarding - NoDatabaseWithEID
	public void rcsmsOnboardingNoDatabaseWithEIDTest() throws Exception {

		// Placeholder given for request body filename in respective json data file.
		// Also printed in console log as test description to differentiate tests
		String testDesc = "NoDatabaseWithEID";

		// Sending POST request
		Response res = rcsms.rcsmsOnboardingTest(jsonDataFile, token, testDesc);

		// Response body validation 
		if (res.path("result.code").equals(3) && res.path("result.message").equals("NO TRUSTED DATABASE RECORD")) {
			
			System.out.println("Onboarding test passed successfully!");
		} else {
			
			Assert.fail("\n\tOnboarding request failed!\n\t" + res.path("result.message") + "\nUnexpected Response!!!");
		}
	}

	@Test
	// RCSMS test for Onboarding - NoDbNfcMatchWithEID
	public void rcsmsOnboardingNoDbNfcMatchWithEIDTest() throws Exception {

		// Placeholder given for request body filename in respective json data file.
		// Also printed in console log as test description to differentiate tests
		String testDesc = "NoDbNfcMatchWithEID";

		// Sending POST request
		Response res = rcsms.rcsmsOnboardingTest(jsonDataFile, token, testDesc);

		// Response body validation 
		if (res.path("result.code").equals(1) && res.path("result.message").equals("MATCH")) {
			
			System.out.println("Onboarding test passed successfully!");
		} else {
			
			Assert.fail("\n\tOnboarding request failed!\n\t" + res.path("result.message") + "\nUnexpected Response!!!");
		}
	}
	
//<****************Passport Scenarios*******************>
	@Test
	// RCSMS test for Onboarding - MatchWithPassport
	public void rcsmsOnboardingMatchWithPassportTest() throws Exception {

		// Placeholder given for request body filename in respective json data file.
		// Also printed in console log as test description to differentiate tests
		String testDesc = "MatchWithPassport";

		// Sending POST request
		Response res = rcsms.rcsmsOnboardingTest(jsonDataFile, token, testDesc);

		// Response body validation
		if (res.path("result.code").equals(1) && res.path("result.message").equals("MATCH")) {
			
			System.out.println("Onboarding test passed successfully!");
		} else {
			
			Assert.fail("\n\tOnboarding request failed!\n\t" + res.path("result.message") + "\nUnexpected Response!!!");
		}
	}

	@Test
	// RCSMS test for Onboarding - NoMatchWithPassport
	public void rcsmsOnboardingNoMatchWithPassportTest() throws Exception {

		// Placeholder given for request body filename in respective json data file.
		// Also printed in console log as test description to differentiate tests
		String testDesc = "NoMatchWithPassport";

		// Sending POST request
		Response res = rcsms.rcsmsOnboardingTest(jsonDataFile, token, testDesc);

		// Response body validation
		if (res.path("result.code").equals(0) && res.path("result.message").equals("NO MATCH")) {
			
			System.out.println("Onboarding test passed successfully!");
		} else {
			
			Assert.fail("\n\tOnboarding request failed!\n\t" + res.path("result.message") + "\nUnexpected Response!!!");
		}
	}

	@Test
	// RCSMS test for Onboarding - NoDatabaseWithPassport
	public void rcsmsOnboardingNoDatabaseWithPassportTest() throws Exception {

		// Placeholder given for request body filename in respective json data file.
		// Also printed in console log as test description to differentiate tests
		String testDesc = "NoDatabaseWithPassport";

		// Sending POST request
		Response res = rcsms.rcsmsOnboardingTest(jsonDataFile, token, testDesc);

		// Response body validation 
		if (res.path("result.code").equals(3) && res.path("result.message").equals("NO TRUSTED DATABASE RECORD")) {
			
			System.out.println("Onboarding test passed successfully!");
		} else {
			
			Assert.fail("\n\tOnboarding request failed!\n\t" + res.path("result.message") + "\nUnexpected Response!!!");
		}
	}

	@Test
	// RCSMS test for Onboarding - NoDbNfcMatchWithPassport
	public void rcsmsOnboardingNoDbNfcMatchWithPassportTest() throws Exception {

		// Placeholder given for request body filename in respective json data file.
		// Also printed in console log as test description to differentiate tests
		String testDesc = "NoDbNfcMatchWithPassport";

		// Sending POST request
		Response res = rcsms.rcsmsOnboardingTest(jsonDataFile, token, testDesc);

		// Response body validation 
		if (res.path("result.code").equals(1) && res.path("result.message").equals("MATCH")) {
			
			System.out.println("Onboarding test passed successfully!");
		} else {
			
			Assert.fail("\n\tOnboarding request failed!\n\t" + res.path("result.message") + "\nUnexpected Response!!!");
		}
	}
	
//<****************Adjudication Scenarios*******************>
	@Test
	// RCSMS test for Adjudication - RejectedList
	public void rcsmsAdjudicationRejList() throws Exception {

		// Placeholder given for request body filename in respective json data file.
		// Also printed in console log as test description to differentiate tests
		String testDesc = "RejectedList";

		// Sending POST request
		Response res = arl.adjudicationRejectList(jsonDataFile, token, testDesc);

		// Response body validation 
		if (res.path("data")!=null) {
			
			System.out.println("Adjudication test passed successfully!");
		} else {
			
			Assert.fail("\n\tAdjudication request failed!\n\t" + res.path("result.message") + "\nUnexpected Response!!!");
		}
	}
	
}
