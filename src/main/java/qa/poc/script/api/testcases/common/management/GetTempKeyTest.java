package qa.poc.script.api.testcases.common.management;

import java.io.IOException;
import java.util.LinkedHashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.response.Response;
import qa.poc.conf.ConfigManager;
import qa.poc.script.api.services.management.GetTemporaryKey;
import qa.poc.utils.ReadJsonDataFile;
import qa.poc.utils.ReadRequestBody;

public class GetTempKeyTest {

	GetTemporaryKey tempKey = new GetTemporaryKey();
	ConfigManager config = new ConfigManager();
	ReadJsonDataFile readJson = new ReadJsonDataFile();
	ReadRequestBody readReq = new ReadRequestBody();
	public LinkedHashMap<String, String> jsonTestData;

	@Test
	public int getTempKeyTest(String fileName, String token) throws IOException {
		
		//declaration
		jsonTestData = readJson.getDataFromJson(fileName);
		String basePath = jsonTestData.get("basePath");
		String endPoint = jsonTestData.get("tempKeyEndPoint");
		String reqUrl = config.getUrl(basePath, endPoint);
		
		//Sending GET request
		Response res = tempKey.getTempKey(reqUrl, token);
		Boolean resBodyExist = res.getHeaders().hasHeaderWithName("Content-Type");
		if(resBodyExist) 
		System.out.println("\n\tResponse Body: \n\t" + res.asString());

		//Response status code validation
		int statusCode = res.getStatusCode();
		if (statusCode != 200)
			Assert.assertEquals(statusCode, 200, "\n\tGet Temporary Key request failed:\n\t Status Code- " + statusCode+"\n\t");
		
		//Returns status code to validate before proceeding to next step
		return statusCode;

	}
}
