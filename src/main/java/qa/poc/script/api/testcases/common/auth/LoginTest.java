package qa.poc.script.api.testcases.common.auth;

import java.util.LinkedHashMap;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.response.Response;
import qa.poc.conf.ConfigManager;
import qa.poc.script.api.services.auth.Login;
import qa.poc.utils.ReadJsonDataFile;
import qa.poc.utils.ReadRequestBody;

public class LoginTest {

	Login login = new Login();
	ConfigManager config = new ConfigManager();
	ReadJsonDataFile readJson = new ReadJsonDataFile();
	ReadRequestBody readReq = new ReadRequestBody();
	public LinkedHashMap<String, String> jsonTestData;

	@Test
	public String loginTest(String fileName) throws Exception {
		
		//declaration & initialization
		String token = null;
		jsonTestData = readJson.getDataFromJson(fileName);
		System.out.println("\tjson Test Data file content: " + jsonTestData.toString());
		String basePath = jsonTestData.get("basePath");
		String endPoint = jsonTestData.get("loginEndPoint");
		String reqUrl = config.getUrl(basePath, endPoint);
		String reqBody = readReq.getContent(jsonTestData.get("loginReqBody"));
		
		//Sending POST request
		Response res = login.login(reqBody, reqUrl);
		Boolean resBodyExist = res.getHeaders().hasHeaderWithName("Content-Type");
		if(resBodyExist) 
		System.out.println("\n\tResponse Body: \n\t" + res.asString());

		//Response status code validation
		int statusCode = res.getStatusCode();
		if (statusCode != 200 && resBodyExist) {
			Assert.assertEquals(statusCode, 200,
					"\n\tLogin request failed:\n\t Status Code- " + statusCode + "\n\t Error Code- "
							+ res.path("errors.code") + "]\n\t Error Message- " + res.path("errors.message")+"\n\t");
		} else {
			Assert.assertEquals(statusCode, 200,
					"\n\tOnboarding request failed:\n\t Status Code- " + statusCode +"\n\t");
		}
		//Response body validation and Token extraction from response body
		if (res.path("succeeded").equals(true)) {

			token = res.path("data.token");
			System.out.println("\n\tToken:\t" + token);
		} else {
			Assert.fail("\n\tLogin request failed with error:\n\t" + res.path("errors")+"\n");
		}

		//Returns Token for authorization
		return token;
	}
}
