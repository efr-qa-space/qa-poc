package qa.poc.adapters;

import io.restassured.response.Response;
import static io.restassured.RestAssured.given;

public class RestAssuredWrapper {
	/**
	 * RestAssuredWrapper class
	 * Has GET, POST methods using RestAssured
	 * Other required http methods should be written in this class as per requirement
	 * */
	public Response getRequest (String url, String token) {
		
		Response response = given()
				.header("Authorization","Bearer "+token)
				.when()
				.get(url)
				.then()
				.extract()
				.response();
		
		return response;
	}
	
	//post request for Login request i.e., without token parameter
	public Response postRequest (String url, String requestBody) {
		
		Response response = given()
                .header("Content-type", "application/json")
                .and()
                .body(requestBody)
                .when()
                .post(url)
                .then()
                .extract().response();
		
		return response;
	}
	
	public Response postRequest (String url, String token, String requestBody) {
		
		Response response = given()
                .header("Content-type", "application/json").header("Authorization","Bearer "+token)
                .and()
                .body(requestBody)
                .when()
                .post(url)
                .then()
                .extract().response();
		
		return response;
	}
}
