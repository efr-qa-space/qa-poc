package qa.poc.application;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import qa.poc.script.api.testcases.rcsms.RCSMSRegressionTest;
import qa.poc.utils.ASCIIArtGenerator;
import qa.poc.utils.ASCIIArtGenerator.ASCIIArtFont;

public class EfrQaLauncher {

	public static void main(String[] args) throws Exception {
		
		/**
	     * EfrQaLauncher : Application entry point.
		*/
		System.setProperty("https.protocols", "TLSv1.3");
		//java.lang.System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
		//Create a new log
		//configureLogger();
		//System.setOut(new PrintStream(new File("./logs/EFR-QA"+currentDateTime()+".log")));
		
		EfrQaLogger.info("Launching EFR-QA Test Suite...");
		ASCIIArtGenerator artGen = new ASCIIArtGenerator();
        artGen.printTextArt("EFR-QA", ASCIIArtGenerator.ART_SIZE_HUGE, ASCIIArtFont.ART_FONT_SERIF,"#");
		
		RCSMSRegressionTest rcsms = new RCSMSRegressionTest();
		rcsms.RCSMSRegression();

	}

	private static void configureLogger() {
        try (FileWriter writer = new FileWriter(new File("./logs/EFR-QA_log-"+currentDateTime()+".log"))) {
            // This file is specified in log4j properties
            writer.write("");
            // EfrQaLogger.setLogger(LogManager.getLogger(EfrQaLauncher.class));
        } catch (IOException e) {
            // TestNG Reporter - ReportNG log will work.
            EfrQaLogger.debug(e);
        } catch (Exception e) {
            // TestNG Reporter - ReportNG log will work.
        	EfrQaLogger.debug(e);
        }
	}
	
	public static String currentDateTime() {
		
		   DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");  
		   LocalDateTime now = LocalDateTime.now();
		   
		return dtf.format(now);
	}
}
