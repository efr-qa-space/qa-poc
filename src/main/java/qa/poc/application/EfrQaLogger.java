package qa.poc.application;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Reporter;


/*
 * Most possible methods are given here for future modifications only.
 * Not a heavily customized logger. Just a wrapper for Reporter for now.
 */
public class EfrQaLogger {

    private static Logger logger = LogManager.getLogger(EfrQaLogger.class);

    private EfrQaLogger() {
        throw new IllegalStateException("Constant class - cannot be instantiated");
    }

    public static void info(String message) {
        logger = LogManager.getLogger(Thread.currentThread().getStackTrace()[2].getClassName());
        logger.info(message);
        log(message, 3);
    }

    public static void debug(String message) {
        logger = LogManager.getLogger(Thread.currentThread().getStackTrace()[2].getClassName());
        logger.debug(message);
        log(message, 4);
    }

    public static void debug(Exception e) {
        logger = LogManager.getLogger(Thread.currentThread().getStackTrace()[2].getClassName());
        logger.debug(e);
        debug("Exception= " + e.getMessage());
        StringWriter errors = new StringWriter();
        e.printStackTrace((new PrintWriter(errors)));
        debug(errors.toString());
    }

    public static void warn(String message) {
        logger = LogManager.getLogger(Thread.currentThread().getStackTrace()[2].getClassName());
        logger.warn(message);
        log(message, 2);
    }

    public static void error(String message) {
        logger = LogManager.getLogger(Thread.currentThread().getStackTrace()[2].getClassName());
        logger.error(message);
        log(message, 1);
    }

    public static void error(Exception e) {
        logger = LogManager.getLogger(Thread.currentThread().getStackTrace()[2].getClassName());
        logger.error(e);
        error("Exception= " + e.getMessage());
        StringWriter errors = new StringWriter();
        e.printStackTrace((new PrintWriter(errors)));
        error(errors.toString());
    }

    public static void log(String s) {
        // This and error() are typically same
        // Created for user friendliness
        logger = LogManager.getLogger(Thread.currentThread().getStackTrace()[2].getClassName());
        logger.info(s);
        Reporter.log(s, 1, true);
    }

    public static void log(Exception e) {
        StringWriter errors = new StringWriter();
        e.printStackTrace((new PrintWriter(errors)));
        Reporter.log(errors.toString(), 1, true);
    }

    private static void log(String s, int level) {
        Reporter.log(s, level, true);
    }

}
