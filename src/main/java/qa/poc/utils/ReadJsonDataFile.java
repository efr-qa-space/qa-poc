package qa.poc.utils;

import java.io.File;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;

import org.testng.annotations.Test;
import org.testng.reporters.Files;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

public class ReadJsonDataFile {

	private LinkedHashMap<String, String> JsonTestData = null;

	public ReadJsonDataFile() {
		JsonTestData = new LinkedHashMap<String, String>();
	}

	public LinkedHashMap<String, String> getDataFromJson(String fileName) {

		this.readTestDetailsFromJson("./src/main/resources/datafiles/TestData", fileName);
		return JsonTestData;
	}

	@SuppressWarnings("unchecked")
	public void readTestDetailsFromJson(String dataFilesFolder, String fileName) {

		File directory = new File(dataFilesFolder);
		try {
			// Get all files from the directory.
			File[] fList = directory.listFiles();
			if (fList != null)
				for (File file : fList) {
					if (file.isFile()) {
						Gson gson = new Gson();
						if (file.getName().contains(fileName)) {
							LinkedHashMap<String, String> lhmTcParams = gson.fromJson(Files.readFile(file),
									LinkedHashMap.class);

							for (Map.Entry<String, String> tcEntry : lhmTcParams.entrySet()) {
								JsonTestData.put(tcEntry.getKey(), tcEntry.getValue());
							}
						}
					}
				}
		} catch (Exception e) {
			System.out.println("Exception occured while processing Json test Sources");
			e.printStackTrace();
		}
	}
}
