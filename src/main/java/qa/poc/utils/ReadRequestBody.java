package qa.poc.utils;

import java.nio.file.Files;
import java.nio.file.Paths;

public class ReadRequestBody {
	
	public String getContent(String fileName) throws Exception {
		
		System.out.println("Request body file path: src/main/resources/datafiles/RequestBody/"+fileName);
		String reqBody = new String(Files.readAllBytes(Paths.get("./src/main/resources/datafiles/RequestBody/"+fileName)));
		return reqBody;
	}
}
